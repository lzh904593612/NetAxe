<a href='https://gitee.com/IFLY-DevNet/net-axe/stargazers'><img src='https://gitee.com/IFLY-DevNet/net-axe/badge/star.svg?theme=dark' alt='star'></img></a>
<a href='https://gitee.com/IFLY-DevNet/net-axe/members'><img src='https://gitee.com/IFLY-DevNet/net-axe/badge/fork.svg?theme=white' alt='fork'></img></a>

[![IFLY-DevNet/NetAxe](https://gitee.com/IFLY-DevNet/net-axe/widgets/widget_card.svg?colors=2877c7,e0e0e0,bddcff,e3e9ed,666666,9b9b9b)](https://gitee.com/IFLY-DevNet/net-axe)




NetAxe 是一个让NetDevOps更简单、更快速、更高效的快速集成前后端一体化框架。

📚 [NetAxe 文档](https://netaxe.github.io/) : https://netaxe.github.io/



## 平台架构图

![平台架构图](https://www.hualigs.cn/image/6333050e344c7.jpg)
##  1.平台登录页

![登录页面](https://www.hualigs.cn/image/6332685964f33.jpg)

##  2.资产管理
![资产管理](https://www.hualigs.cn/image/633268f86cb1a.jpg)

##  3.配置差异比较
![配置差异比较](https://www.hualigs.cn/image/63326943da30d.jpg)

##  4.Webssh
![Webssh](https://www.hualigs.cn/image/63326a5be2bf5.jpg)

##  5.接口清单

![接口清单](https://www.hualigs.cn/image/63326aefc16ea.jpg)

##  6.采集方案
![采集方案](https://www.hualigs.cn/image/63326e00dcaf5.jpg)

##  7.任务列表
![任务列表](https://www.hualigs.cn/image/63326e4b50c51.jpg)

##  8.任务调度管理
![任务调度管理](https://www.hualigs.cn/image/63326ef012392.jpg)



## 交流
![NetAxe开源社区](https://www.hualigs.cn/image/633f8a64a8910.jpg)
群名称:NetAxe开源社区

## 特别感谢
感谢伟大的Django、VUE、vue-admin-work
- [Django](https://github.com/django/django)
- [VUE](https://github.com/vuejs/vue)
- [vue-admin-work](https://github.com/qingqingxuan/vue-admin-work)